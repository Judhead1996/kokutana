    import { Component } from '@angular/core';
    //import { NavController } from 'ionic-angular';
    //import { Http } from '@angular/http';
    import { HttpClient } from '@angular/common/http';
    import { ReactiveFormsModule } from '@angular/forms';

    @Component({
      selector: 'page-home',
      templateUrl: 'home.page.html'
    })
    
    export class HomePage {
      email:''
      isEmpty=false
      loader=false
      nexistepas=false
      pasInternet=false
      presenceConfirmer=false
      isAjouter=false
      prenom:''
      nom:''
      //api_base_url= "http://127.0.0.1:8000/api/";
      api_base_url: "http://51.254.98.35:8000/api/"

      constructor(public http:HttpClient) {
      }
    
      Retour()
      {
        this.nexistepas=false
      }
      validation(item){
        if(item!=undefined && item!=''){
        
          return true
        }
        else{
          this.isEmpty=true
          this.pasInternet=false
          this.nexistepas=false
          setTimeout(() => {
            this.isEmpty=false
          }, 5000)
          return false;
        }
      }

      addParticipant(){
        let participant = {
          email: this.email,
          first_name: this.prenom,
          last_name:this.nom,
        }
        this.loader=true
        this.http.post("http://51.254.98.35:8000/api/kokutana-presences", participant, {})
        .subscribe(data => {
          this.nexistepas=false
          this.isAjouter=true
          this.loader=false
          this.email=""
          this.prenom=""
          this.nom=""
          participant={
            email: '',
            first_name: '',
            last_name:'',
          }
          setTimeout(() => {
            this.isAjouter=false
          }, 4000)
          console.log(data)
        }, error => {
          this.isAjouter=false
          this.loader=false
          console.log(error);
        });
      }

      
      onSubmitPresenceTrue(){
        console.log("http://51.254.98.35:8000/api/presence-checking-true")
        let mail = {
          email: this.email,
        }
        if(this.validation(this.email)){
          this.loader=true
          this.pasInternet=false
          this.nexistepas=false
          this.presenceConfirmer=false
          this.http.post("http://51.254.98.35:8000/api/presence-checking-true", mail, {})
            .subscribe(data => {
              console.log((<any>data).data);
              this.prenom = (<any>data).data.first_name;
              this.loader=false
              this.email=''
              this.isEmpty=false
              this.presenceConfirmer=true
              setTimeout(() => {
                this.presenceConfirmer=false
              }, 4000)
              
            }, error => {
              this.loader=false
              if(error.statusText==""){
                this.pasInternet=true
                this.isEmpty=false
              }else{
                this.nexistepas=true;
                this.isEmpty=false;
              }
        });
      }    
    }
   
}


